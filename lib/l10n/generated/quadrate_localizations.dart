import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'quadrate_localizations_en.dart';
import 'quadrate_localizations_ru.dart';

/// Callers can lookup localized strings with an instance of QuadrateLocalization
/// returned by `QuadrateLocalization.of(context)`.
///
/// Applications need to include `QuadrateLocalization.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'generated/quadrate_localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: QuadrateLocalization.localizationsDelegates,
///   supportedLocales: QuadrateLocalization.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the QuadrateLocalization.supportedLocales
/// property.
abstract class QuadrateLocalization {
  QuadrateLocalization(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static QuadrateLocalization? of(BuildContext context) {
    return Localizations.of<QuadrateLocalization>(context, QuadrateLocalization);
  }

  static const LocalizationsDelegate<QuadrateLocalization> delegate = _QuadrateLocalizationDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('en'),
    Locale('ru')
  ];

  /// No description provided for @error.
  ///
  /// In en, this message translates to:
  /// **'Error'**
  String get error;

  /// No description provided for @connectionFailure.
  ///
  /// In en, this message translates to:
  /// **'Connection failure'**
  String get connectionFailure;

  /// No description provided for @checkNetworkAndRetry.
  ///
  /// In en, this message translates to:
  /// **'Check network connection and retry'**
  String get checkNetworkAndRetry;

  /// No description provided for @serverFailure.
  ///
  /// In en, this message translates to:
  /// **'Server failure'**
  String get serverFailure;

  /// No description provided for @tryAgain.
  ///
  /// In en, this message translates to:
  /// **'Please try again in few moments'**
  String get tryAgain;
}

class _QuadrateLocalizationDelegate extends LocalizationsDelegate<QuadrateLocalization> {
  const _QuadrateLocalizationDelegate();

  @override
  Future<QuadrateLocalization> load(Locale locale) {
    return SynchronousFuture<QuadrateLocalization>(lookupQuadrateLocalization(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['en', 'ru'].contains(locale.languageCode);

  @override
  bool shouldReload(_QuadrateLocalizationDelegate old) => false;
}

QuadrateLocalization lookupQuadrateLocalization(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'en': return QuadrateLocalizationEn();
    case 'ru': return QuadrateLocalizationRu();
  }

  throw FlutterError(
    'QuadrateLocalization.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
