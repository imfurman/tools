import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:quadrate_tools/service/dispose_helper.dart';
import 'package:rxdart/rxdart.dart';

/// Тип для функции валидации значения
typedef Validator<T, V> = V? Function(T value);

/// Контроллер поля на форме.
///
/// Позволяет получать от поля уведомления об изменении значения.
/// Позволяет валидировать значение поля и транслировать сообщение об ошибке валидации.
class PropertyController<T, V> with DisposeHelper {
  /// Позволяет добавить мета-информацию о значении.
  /// Например "Имя", "Номер телефона". В зависимости от этого отображать нужный UI для редактирования поля.
  final Object? valueType;
  final String caption;
  final String? comment;

  /// Показывает, что для контроллера нужно сделать визуальное поле ввода
  final bool userEnter;
  late BehaviorSubject<T> _value;
  late BehaviorSubject<TextSpan> _formattedValue;
  late BehaviorSubject<V?> _validationMsgOutput;
  late BehaviorSubject<bool> _isValidOutput;
  late BehaviorSubject<bool> _isDataChangedOutput;
  late BehaviorSubject<bool> _showValidationMsg;
  final Validator<T, V> _validator;
  late TextSpan Function(T) _valueFormatter;
  late T Function(T) _valueFilter;

  PropertyController(T initValue,
      {this.caption = '',
      this.comment,
      this.valueType,
      this.userEnter = true,
      bool showValidationMsg = true,
      Validator<T, V>? validator,
      TextSpan Function(T)? valueFormatter,
      T Function(T)? valueFilter})
      : _validator = (validator ?? (anyValue) => null) {
    _valueFilter = valueFilter ?? (value) => value;
    _valueFormatter = valueFormatter ?? (T param) => TextSpan(text: param?.toString() ?? '');
    final filteredValue = _valueFilter(initValue);
    addSubject(_value = BehaviorSubject<T>.seeded(filteredValue));
    addSubject(_formattedValue = BehaviorSubject<TextSpan>.seeded(_valueFormatter(filteredValue)));
    addSubject(_showValidationMsg = BehaviorSubject<bool>.seeded(showValidationMsg));
    addSubject(_validationMsgOutput = BehaviorSubject<V?>());
    addSubject(_isValidOutput = BehaviorSubject<bool>.seeded(_validator(filteredValue) == null));
    addSubject(_isDataChangedOutput = BehaviorSubject<bool>.seeded(false));

    addSubscription(_value.distinct().skip(1).listen((data) {
      _isDataChangedOutput.add(true);
    }));

    addSubscription(_value.skip(1).listen((data) {
      _formattedValue.add(_valueFormatter(data));
      _isValidOutput.add(_validator(data) == null);
    }));

    addSubscription(CombineLatestStream.combine2<T, bool, V?>(
        _value, _showValidationMsg, (propertyValue, showValidationMsgValue) {
      return showValidationMsgValue ? _validator(propertyValue) : null;
    }).listen((value) {
      _validationMsgOutput.add(value);
    }));
  }

  PropertyController<T, V> clone() {
    return PropertyController<T, V>(value,
        caption: caption,
        valueType: valueType,
        showValidationMsg: _showValidationMsg.value,
        validator: _validator,
        valueFormatter: _valueFormatter,
        valueFilter: _valueFilter);
  }

  TextSpan Function(T value) get valueFormatter => _valueFormatter;

  /// this may be needed when injected validator depend on some other value
  /// so validation
  /// result may change not just because of internal value change.
  void validate() {
    _value.add(_value.value);
  }

  T get value => _value.value;

  set value(T value) {
    _value.add(_valueFilter(value));
  }

  Stream<T> get valueOutput => _value;

  Stream<TextSpan> get formatted => _formattedValue;

  TextSpan get formattedValue => _formattedValue.value;

  Stream<V?> get validationMsg => _validationMsgOutput;

  BehaviorSubject<bool> get isValid => _isValidOutput;

  bool get isValidValue => _isValidOutput.value;

  BehaviorSubject<bool> get isDataChanged => _isDataChangedOutput;

  Stream<bool> get showValidationMsg => _showValidationMsg;

  bool get showValidationMsgValue => _showValidationMsg.value;

  set showValidationMsgValue(bool value) {
    _showValidationMsg.add(value);
  }

  bool get isMandatory {
    return !(null is T && _validator(null as T) == null);
  }
}
