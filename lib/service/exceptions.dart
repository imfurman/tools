class AppException implements Exception {
  final dynamic title;
  final dynamic description;
  final dynamic cause;

  AppException([this.title, this.description, this.cause]);

  @override
  String toString() {
    return title?.toString() ?? 'Ошибка в работе приложения';
  }
}