import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quadrate_tools/service/property_controller.dart';
import 'package:quadrate_tools/widget/immutable_text_form_field.dart';
import 'package:quiver/strings.dart';

class FormattedNumberPropertyFormField<V> extends StatelessWidget {
  final PropertyController<String?, V> propertyController;
  final InputDecoration _decoration;
  final String inputPattern;
  final bool autofocus;
  final TextStyle? style;
  final TextAlign textAlign;
  final ErrorMessageMapper<V> _errorMessageMapper;

  const FormattedNumberPropertyFormField(this.inputPattern,
      {required this.propertyController,
      InputDecoration? decoration,
      this.autofocus = false,
      this.style,
      this.textAlign = TextAlign.start,
      ErrorMessageMapper<V>? errorMessageMapper})
      : _decoration = decoration ?? const InputDecoration(),
        _errorMessageMapper = errorMessageMapper ?? defaultErrorMessageMapper<V>;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String?>(
        stream: propertyController.validationMsg
            .distinct()
            .map((errorMsg) => _errorMessageMapper(context, errorMsg)),
        builder: (context, errorMsgSnapshot) {
          return FormattedNumberFormField(inputPattern,
              textAlign: textAlign,
              autofocus: autofocus,
              style: style,
              initValue: propertyController.value,
              decoration: _decoration.copyWith(errorText: errorMsgSnapshot.data), onChanged: (newValue) {
            propertyController.value = newValue;
          });
        });
  }
}

class FormattedNumberFormField extends StatefulWidget {
  final String inputPattern;
  final String? dialPrefix;
  final bool autofocus;
  final VoidCallback? onLostFocus;
  final ValueChanged<String?>? onChanged;
  final String? initValue;
  final String? errorText;
  final InputDecoration? decoration;
  final TextStyle? style;
  final TextInputAction textInputAction;
  final ValueChanged<String>? onSubmitted;
  final TextAlignVertical? textAlignVertical;
  final bool obscureText;
  final TextAlign textAlign;

  const FormattedNumberFormField(this.inputPattern,
      {Key? key,
      this.dialPrefix,
      this.onLostFocus,
      this.initValue = '',
      this.errorText,
      this.onChanged,
      this.decoration,
      this.autofocus = false,
      this.obscureText = false,
      this.style,
      this.textInputAction = TextInputAction.next,
      this.onSubmitted,
      this.textAlignVertical,
      this.textAlign = TextAlign.start})
      : super(key: key);

  @override
  _FormattedNumberFormFieldState createState() => _FormattedNumberFormFieldState();
}

class _FormattedNumberFormFieldState extends State<FormattedNumberFormField> {
  final focusNode = FocusNode();
  late TextEditingController textController;
  late NumberTextInputFormatter numberTextInputFormatter;

  @override
  void initState() {
    super.initState();
    numberTextInputFormatter =
        NumberTextInputFormatter(widget.inputPattern, dialPrefix: widget.dialPrefix);
    final initValue = (widget.initValue ?? '').replaceAll(RegExp(r'\D'), '');
    final textEditingValue =
        TextEditingValue(text: initValue, selection: TextSelection.collapsed(offset: initValue.length));
    textController = TextEditingController.fromValue(isEmpty(initValue)
        ? textEditingValue
        : numberTextInputFormatter.formatEditUpdate(null, textEditingValue));
    focusNode.addListener(() {
      if (!focusNode.hasFocus && widget.onLostFocus != null) {
        widget.onLostFocus!();
      }
    });
    textController.addListener(() {
      if (widget.onChanged != null) {
        widget.onChanged!(isEmpty(textController.text) ? null : textController.text);
      }
    });
    if (widget.autofocus) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        focusNode.requestFocus();
      });
    }
  }

  @override
  void dispose() {
    focusNode.dispose();
    textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        autofocus: widget.autofocus,
        obscureText: widget.obscureText,
        textAlignVertical: widget.textAlignVertical,
        style: widget.style,
        controller: textController,
        focusNode: focusNode,
        decoration:
            widget.decoration ?? InputDecoration(labelText: 'Телефон', errorText: widget.errorText),
        keyboardType: TextInputType.number,
        autocorrect: false,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly,
          numberTextInputFormatter
        ],
        textInputAction: widget.textInputAction,
        onFieldSubmitted: widget.onSubmitted,
        textAlign: widget.textAlign);
  }
}

/// Форматтер числа по шаблону.
///
/// В качестве шаблона принимает строку, где символом '#' отмечены места для цифр.
///
/// На входе метода [formatEditUpdate] ожидает строку, состоящую
/// из одних только цифр. В списке форматтеров ставить его после
/// WhitelistingTextInputFormatter.digitsOnly, который сохраняет положение курсора.
class NumberTextInputFormatter extends TextInputFormatter {
  final Pattern _placeholderPattern = RegExp(r'(.*?)#');
  final String _pattern;
  final String? _dialPrefix;

  NumberTextInputFormatter(String pattern, {String? dialPrefix})
      : _pattern = pattern,
        _dialPrefix = dialPrefix;

  @override
  TextEditingValue formatEditUpdate(TextEditingValue? oldValue, TextEditingValue newValue) {
    var selectionStart = max<int>(newValue.selection.start, 0);
    var digitsOnly = newValue.text;
    if (isNotEmpty(digitsOnly) && isNotEmpty(_dialPrefix)) {
      if (!digitsOnly.startsWith(_dialPrefix!)) {
        digitsOnly = '$_dialPrefix$digitsOnly';
        selectionStart++;
      }
    }
    final sb = StringBuffer();
    var position = 0;
    var adjustedSelectionStart = selectionStart;
    for (var value in _placeholderPattern.allMatches(_pattern)) {
      if (position < digitsOnly.length) {
        final group1 = value.group(1);
        if (position == selectionStart) {
          adjustedSelectionStart = sb.length;
        }
        sb.write(group1);
        sb.write(digitsOnly.substring(position, position + 1));
        position++;
      } else {
        break;
      }
    }
    if (selectionStart >= position) {
      adjustedSelectionStart = sb.length;
    }
    return TextEditingValue(
        text: sb.toString(), selection: TextSelection.collapsed(offset: adjustedSelectionStart));
  }
}
