import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:quadrate_tools/widget/extended_state.dart';
import 'package:rxdart/rxdart.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:visibility_detector/visibility_detector.dart';

const double kTabHeight = 46.0;

class TabbedListView<T> extends StatefulWidget {
  final Iterable<T> items;
  final Widget Function(BuildContext context, T item) itemBuilder;
  final Widget Function(BuildContext context, T item) tabBuilder;
  final Widget? appBarTitle;
  final Widget? appBarLeading;
  final double? appBarLeadingWidth;
  final double? appBarHeight;
  final ShapeBorder? appBarShape;
  final EdgeInsetsGeometry? appBarPadding;
  final bool appBarFloating;
  final bool appBarSnap;
  final bool appBarCenterTitle;
  final List<Widget>? appBarActions;

  const TabbedListView(
      {Key? key,
      required this.items,
      required this.itemBuilder,
      this.appBarLeading,
      this.appBarLeadingWidth,
      this.appBarTitle,
      this.appBarHeight,
      this.appBarCenterTitle = true,
      this.appBarShape,
      this.appBarPadding,
      required this.tabBuilder,
      this.appBarFloating = true,
      this.appBarSnap = true,
      this.appBarActions})
      : super(key: key);

  @override
  State<TabbedListView<T>> createState() => _TabbedListViewState<T>();
}

class _TabbedListViewState<T> extends ExtendedState<TabbedListView<T>> with TickerProviderStateMixin {
  TabController? tabController;
  final scrollController = AutoScrollController();
  late final Subject<VisibilityInfoWrapper> scrollItemVisibility;
  late final BehaviorSubject<bool> tabPressScrollRunning;

  @override
  void didUpdateWidget(covariant TabbedListView<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    const comparator = IterableEquality();
    if (!comparator.equals(widget.items, oldWidget.items)) {
      tabController?.dispose();
      tabController = TabController(length: widget.items.length, vsync: this);
    }
  }

  @override
  void initState() {
    super.initState();
    addSubject(scrollItemVisibility = PublishSubject<VisibilityInfoWrapper>());
    addSubject(tabPressScrollRunning = BehaviorSubject<bool>.seeded(false));

    tabController = TabController(length: widget.items.length, vsync: this);

    addSubscription(tabPressScrollRunning
        .delay(const Duration(milliseconds: 500))
        .switchMap<VisibilityInfoWrapper>(
            (isRunning) => isRunning ? NeverStream() : scrollItemVisibility)
        .listen((infoWrapper) {
      final currentOffset = scrollController.offset;
      final ctx = scrollController.tagMap[infoWrapper.index]?.context;
      if (ctx != null) {
        final revealedOffset = _offsetToRevealInViewport(ctx, 0.0);
        final localTabController = tabController;
        final shiftedCurrentOffset = currentOffset + 80;
        if (shiftedCurrentOffset > revealedOffset.offset &&
            shiftedCurrentOffset < revealedOffset.offset + infoWrapper.info.size.height &&
            localTabController != null &&
            localTabController.index != infoWrapper.index) {
          localTabController.animateTo(infoWrapper.index,
              duration: const Duration(milliseconds: 200), curve: Curves.easeInOut);
        }
      }
    }));
  }

  @override
  void dispose() {
    tabController?.dispose();
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget buildContent(BuildContext context) {
    final themeData = Theme.of(context);

    return DefaultTabController(
        length: widget.items.length, child: _buildCustomScrollView(themeData, widget.items));
  }

  Widget _buildCustomScrollView(ThemeData themeData, Iterable<T> positionsSnapshotData) {
    return CustomScrollView(controller: scrollController, slivers: [
      _buildSliverAppBar(themeData, positionsSnapshotData),
      SliverList(
          delegate: SliverChildBuilderDelegate(
              _buildIndexedItemBuilder(themeData, positionsSnapshotData),
              childCount: positionsSnapshotData.length))
    ]);
  }

  IndexedWidgetBuilder _buildIndexedItemBuilder(ThemeData themeData, Iterable<T> positionsSnapshotData) {
    return (BuildContext context, int index) {
      final section = positionsSnapshotData.elementAt(index);
      return AutoScrollTag(
          key: ValueKey(index),
          controller: scrollController,
          index: index,
          child: VisibilityDetector(
              key: ValueKey(index),
              onVisibilityChanged: (VisibilityInfo info) {
                if (!scrollItemVisibility.isClosed) {
                  scrollItemVisibility.add(VisibilityInfoWrapper(index: index, info: info));
                }
              },
              child: widget.itemBuilder(context, section)));
    };
  }

  Widget _buildSliverAppBar(ThemeData themeData, Iterable<T> positionsSnapshotData) {
    return SliverAppBar(
        centerTitle: widget.appBarCenterTitle,
        shape: widget.appBarShape,
        title: widget.appBarTitle,
        leading: widget.appBarLeading,
        leadingWidth: widget.appBarLeadingWidth,
        floating: widget.appBarFloating,
        pinned: true,
        snap: widget.appBarSnap,
        actions: widget.appBarActions,
        bottom: positionsSnapshotData.length < 2
            ? const PreferredSize(preferredSize: Size.fromHeight(0), child: SizedBox.shrink())
            : PreferredSize(
                preferredSize: Size.fromHeight(widget.appBarHeight ?? kTabHeight),
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: TabBar(
                        controller: tabController,
                        padding: widget.appBarPadding ?? const EdgeInsets.fromLTRB(16, 6, 16, 10),
                        isScrollable: true,
                        tabs: positionsSnapshotData
                            .map<Widget>((section) => widget.tabBuilder(context, section))
                            .toList(),
                        onTap: (sectionIndex) async {
                          tabPressScrollRunning.add(true);
                          await scrollController.scrollToIndex(
                            sectionIndex,
                            preferPosition: AutoScrollPosition.begin,
                          );
                          tabPressScrollRunning.add(false);
                        }))));
  }

  // Method copied from scroll_to_index lib
  //
  /// return offset, which is a absolute offset to bring the target index object into the center of the viewport
  /// see also: _directionalOffsetToRevealInViewport()
  RevealedOffset _offsetToRevealInViewport(BuildContext ctx, double alignment) {
    final renderBox = ctx.findRenderObject()!;
    final RenderAbstractViewport viewport = RenderAbstractViewport.of(renderBox);
    return viewport.getOffsetToReveal(renderBox, alignment);
  }
}

class VisibilityInfoWrapper {
  final VisibilityInfo info;
  final int index;

  VisibilityInfoWrapper({required this.info, required this.index});
}
