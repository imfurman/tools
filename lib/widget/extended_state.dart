import 'dart:async';

import 'package:flutter/material.dart';
import 'package:quadrate_tools/widget/error_message_widget.dart';
import 'package:rxdart/rxdart.dart';

/// Базовый класс [State] с обработкой флага [loading] и сообщения об ошибке
abstract class ExtendedState<T extends StatefulWidget> extends State<T> {
  ErrorMessage? _errorMessage;
  bool _loading = false;
  String? _subtitleSuffix;
  CompositeSubscription? _subscriptions;
  final List<Subject> _subjects = [];

  Future<void> wrapLoader(Future Function() callback,
      {bool doRetry = true, VoidCallback? onClose}) async {
    try {
      errorMessage = null;
      loading = true;
      await callback();
    } catch (e) {
      if (mounted) {
        errorMessage = ErrorMessage.forException(context, e,
            onReload: doRetry
                ? () {
                    wrapLoader(callback);
                  }
                : null,
            onClose: doRetry
                ? null
                : () {
                    errorMessage = null;
                    if (onClose != null) {
                      onClose();
                    }
                  });
      }
      // the next line is because Crashlytics doesn't
      // catch exceptions thrown is these conditions
      // unawaited(FirebaseCrashlytics.instance.recordError(e, s));
      rethrow;
    } finally {
      loading = false;
    }
  }

  Widget buildContent(BuildContext context);

  @override
  Widget build(BuildContext context) {
    final content = <Widget>[buildContent(context)];
    if (_loading) {
      content.add(buildLoadingStateOverlay());
    }
    if (errorMessage != null) {
      final _errorMessage = _subtitleSuffix == null
          ? errorMessage!
          : errorMessage!.copyWith(subtitle: (errorMessage?.subtitle ?? '') + _subtitleSuffix!);
      if (_errorMessage.onReload != null) {
        content.add(Positioned.fill(
            child: Container(decoration: BoxDecoration(color: Color.fromARGB(30, 0, 0, 0)))));
      }
      content.add(buildErrorMessage(ErrorMessageWidget(errorMessage: _errorMessage)));
    }
    return buildOuterContent(Stack(children: content));
  }

  Widget buildLoadingStateOverlay() {
    return Positioned.fill(
        child: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(color: Color.fromARGB(30, 0, 0, 0)),
            child: Padding(
                padding: EdgeInsets.only(top: 8, bottom: MediaQuery.of(context).viewInsets.bottom + 8),
                child: CircularProgressIndicator())));
  }

  Widget buildOuterContent(Widget child) => child;

  Widget buildErrorMessage(Widget errorMessage) {
    return Positioned(top: 0, left: 0, right: 0, child: SafeArea(child: errorMessage));
  }

  set subtitleSuffix(String suffix) {
    _subtitleSuffix = suffix;
    if (mounted) {
      setState(() {});
    }
  }

  bool get loading => _loading;

  set loading(bool value) {
    _loading = value;
    if (mounted) {
      setState(() {});
    }
  }

  ErrorMessage? get errorMessage => _errorMessage;

  set errorMessage(ErrorMessage? value) {
    _errorMessage = value;
    _subtitleSuffix = null;
    if (mounted) {
      setState(() {});
    }
  }

  void addSubscription(StreamSubscription subscription) {
    _subscriptions ??= CompositeSubscription();
    _subscriptions?.add(subscription);
  }

  void addSubject(Subject subject) {
    _subjects.add(subject);
  }

  @override
  void dispose() {
    _subscriptions?.dispose();
    _subjects.forEach((subject) => subject.close());
    super.dispose();
  }
}
