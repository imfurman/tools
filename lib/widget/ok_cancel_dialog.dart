import 'package:flutter/material.dart';

enum ConfirmationDialogResult { cancel, ok }

class OkCancelDialog extends StatelessWidget {
  final String title;
  final String? content;
  final String cancelButtonTitle;
  final String okButtonTitle;

  const OkCancelDialog(
      {super.key,
      required this.title,
      required this.cancelButtonTitle,
      required this.okButtonTitle,
      this.content});

  @override
  Widget build(BuildContext context) {
    final contentLocal = content;
    return AlertDialog(
        title: Text(title),
        content: contentLocal == null ? null : SingleChildScrollView(child: Text(contentLocal)),
        actions: <Widget>[
          TextButton(
              onPressed: () {
                Navigator.pop(context, ConfirmationDialogResult.cancel);
              },
              child: Text(cancelButtonTitle)),
          TextButton(
              onPressed: () {
                Navigator.pop(context, ConfirmationDialogResult.ok);
              },
              child: Text(okButtonTitle))
        ]);
  }
}
