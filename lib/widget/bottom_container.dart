import 'package:flutter/material.dart';

class BottomContainer extends StatelessWidget {
  final Widget child;
  final Color? backgroundColor;
  final double? elevation;
  final EdgeInsetsGeometry? childPadding;

  const BottomContainer(
      {Key? key,
        required this.child,
        this.backgroundColor,
        this.elevation,
        this.childPadding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    return Material(
        elevation: elevation ?? 4,
        color: backgroundColor ?? themeData.cardTheme.color,
        child: SafeArea(
            top: false,
            minimum:  childPadding?.resolve(TextDirection.ltr) ?? EdgeInsets.zero,
            child: child));
  }
}
