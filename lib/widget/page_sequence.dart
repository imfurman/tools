import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quadrate_tools/service/dispose_helper.dart';
import 'package:quadrate_tools/service/form_data.dart';
import 'package:quadrate_tools/service/helpers.dart';
import 'package:quadrate_tools/widget/extended_state.dart';
import 'package:rxdart/rxdart.dart';

class PageSequence extends StatefulWidget {
  final Widget Function(
      {required int currentPage,
      required Iterable<bool> pagesEnabled,
      void Function(int)? onPageTap,
      VoidCallback? onPrevTap,
      VoidCallback? onNextTap}) pageSequenceNavigatorBuilder;

  final Iterable<Page> pages;

  const PageSequence({Key? key, required this.pageSequenceNavigatorBuilder, required this.pages})
      : super(key: key);

  @override
  State<PageSequence> createState() => _PageSequenceState();
}

class _PageSequenceState extends ExtendedState<PageSequence> {
  late final PageSequenceBloc pageSequenceBloc;

  @override
  void initState() {
    super.initState();
    pageSequenceBloc = PageSequenceBloc(widget.pages);
  }

  @override
  Widget buildContent(BuildContext context) {
    return Column(children: [
      Expanded(
          child: StreamBuilderHelper<Widget>(
              stream: pageSequenceBloc.currentPage.map((page) => page.widget),
              builder: (context, currentPageWidget) => currentPageWidget)),
      StreamBuilderHelper<Widget>(
          whenEmpty: const SizedBox.shrink(),
          stream: CombineLatestStream.combine2<int, List<bool>, Widget>(
              pageSequenceBloc.currentPageIndex,
              pageSequenceBloc.pagesValid,
                  (currentPageIndex, pagesValid) =>
                  widget.pageSequenceNavigatorBuilder(
                      currentPage: currentPageIndex,
                      pagesEnabled: pagesValid,
                      onNextTap: currentPageIndex < pagesValid.length - 1 &&
                          (pagesValid[currentPageIndex] ||
                              !pageSequenceBloc.pages
                                  .elementAt(currentPageIndex)
                                  .formData
                                  .showValidationMsgValue)
                          ? () {
                        wrapLoader(() async {
                          final currentPage = pageSequenceBloc.pages.elementAt(currentPageIndex);
                          currentPage.formData.showValidationMsgValue = true;
                          if (currentPage.formData.isValid.value) {
                            await currentPage.onNextTap?.call();
                            pageSequenceBloc.page = ++currentPageIndex;
                          }
                        }, doRetry: false);
                      }
                          : null,
                      onPrevTap: currentPageIndex == 0
                          ? null
                          : () {
                        pageSequenceBloc.page = --currentPageIndex;
                      },
                      onPageTap: (pageIndex) {
                        pageSequenceBloc.page = pageIndex;
                      })),
          builder: (context, widget) => widget)
    ]);
  }
}

class PageSequenceBloc with DisposeHelper {
  final Iterable<Page> _pages;
  final _currentPageIndex = BehaviorSubject<int>.seeded(0);
  BehaviorSubject<bool>? _isDataChanged;

  PageSequenceBloc(Iterable<Page> pages) : _pages = List.unmodifiable(pages);

  Iterable<FormData> get pagesFormData => _pages.map<FormData>((page) => page.formData);

  Iterable<Page> get pages => _pages;

  Stream<Page> get currentPage => _currentPageIndex.map<Page>((index) => _pages.elementAt(index));

  Stream<List<bool>> get pagesValid {
    return CombineLatestStream<bool, List<bool>>(
        pagesFormData.map<Stream<bool>>((formData) => formData.isValid), (values) => values);
  }

  BehaviorSubject<bool> get isDataChanged {
    if (_isDataChanged == null) {
      addSubject(_isDataChanged = BehaviorSubject<bool>.seeded(false));
      addSubscription(combineBooleanStreamsOr(pagesFormData.map<Stream<bool>>((formData) {
        return formData.isDataChanged;
      }), _isDataChanged!));
    }
    return _isDataChanged!;
  }

  BehaviorSubject<int> get currentPageIndex => _currentPageIndex;

  set page(int pageIndex) {
    if (pageIndex == 0) {
      _currentPageIndex.add(pageIndex);
    } else {
      final correctedPageIndex = min<int>(pagesFormData.length - 1, max(pageIndex, 0));
      final previousFormsValid = pagesFormData
          .take(correctedPageIndex)
          .map<bool>((formData) => formData.isValid.value)
          .fold<bool>(true, (previousValue, element) => previousValue && element);
      if (previousFormsValid) {
        _currentPageIndex.add(correctedPageIndex);
      }
    }
  }

  @override
  void dispose() {
    for (var page in _pages) {
      page.formData.dispose();
    }
    _currentPageIndex.close();
    super.dispose();
  }
}

class Page {
  final Widget widget;
  final FormData formData;
  final Future Function()? onNextTap;

  Page({required this.widget, required this.formData, this.onNextTap});
}
