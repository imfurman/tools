import 'dart:io' as io;

import 'package:flutter/material.dart';
import 'package:quadrate_tools/l10n/generated/quadrate_localizations.dart';
import 'package:quadrate_tools/service/exceptions.dart';
import 'package:quiver/strings.dart';

/// Сообщение об ошибке с кнопкой повтора
class ErrorMessageWidget extends StatefulWidget {
  final ErrorMessage errorMessage;
  final Color? backgroundColor;

  const ErrorMessageWidget({
    Key? key,
    this.backgroundColor,
    this.errorMessage = const ErrorMessage(),
  }) : super(key: key);

  @override
  _ErrorMessageWidgetState createState() => _ErrorMessageWidgetState();
}

class _ErrorMessageWidgetState extends State<ErrorMessageWidget>
    with TickerProviderStateMixin {
  static const _actionBtnWidth = 56.0;
  static const _exclamationMarkWidth = 56.0;
  static const _exclamationMarkHeight = 64.0;
  static const _actionBtnHeight = 64.0;
  static const _textVerticalSpacing = 10.0;
  bool _showTechDetails = false;

  @override
  Widget build(BuildContext context) {
    final hasActionButton = widget.errorMessage.hasAction;
    return Material(
        elevation: 6,
        color: Color(0x00FFFFFF),
        child: Ink(
            color: widget.backgroundColor ?? Colors.red,
            child: Stack(children: <Widget>[
              ConstrainedBox(
                  constraints: BoxConstraints(minHeight: _actionBtnHeight),
                  child: InkWell(
                      onTap: isBlank(widget.errorMessage.techDetails)
                          ? null
                          : () {
                              setState(() {
                                _showTechDetails = !_showTechDetails;
                              });
                            },
                      child: Padding(
                          padding: EdgeInsets.only(
                              right: hasActionButton ? _actionBtnWidth : 0,
                              left: _exclamationMarkWidth),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                ConstrainedBox(
                                    constraints: BoxConstraints(
                                        minHeight: _actionBtnHeight),
                                    child: Center(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: _textVerticalSpacing),
                                            child: Text(
                                                widget.errorMessage.title ?? '',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                          ),
                                          if (isNotBlank(
                                              widget.errorMessage.subtitle))
                                            Padding(
                                                padding: const EdgeInsets.only(
                                                    bottom:
                                                        _textVerticalSpacing),
                                                child: Text(
                                                    widget
                                                        .errorMessage.subtitle!,
                                                    style: TextStyle(
                                                        color: Colors.white))),
                                        ],
                                      ),
                                    )),
                                if (isNotBlank(widget.errorMessage.techDetails))
                                  SizeChangedLayoutNotifier(
                                    child: AnimatedSize(
                                        duration: Duration(milliseconds: 300),
                                        child: _showTechDetails
                                            ? Padding(
                                                padding: EdgeInsets.only(
                                                  bottom: _textVerticalSpacing,
                                                ),
                                                child: Text(
                                                    widget.errorMessage
                                                        .techDetails!,
                                                    style: TextStyle(
                                                        color: Colors.white, fontSize: 11)))
                                            : Container()),
                                  )
                              ])))),
              if (hasActionButton)
                Positioned(
                    right: 0,
                    child: InkResponse(
                      onTap: widget.errorMessage.onClose ??
                          widget.errorMessage.onReload,
                      child: SizedBox(
                          width: _actionBtnWidth,
                          height: _actionBtnHeight,
                          child: Icon(
                              widget.errorMessage.onClose == null
                                  ? Icons.autorenew
                                  : Icons.close,
                              color: Colors.white)),
                    )),
              Positioned(
                  left: 0,
                  child: IgnorePointer(
                      child: SizedBox(
                          width: _exclamationMarkWidth,
                          height: _exclamationMarkHeight,
                          child: Icon(Icons.error, color: Colors.white))))
            ])));
  }
}

class ErrorMessage {
  final String? title;
  final String? subtitle;
  final String? techDetails;
  final VoidCallback? onReload;
  final VoidCallback? onClose;

  const ErrorMessage(
      {this.title = 'Error',
      this.subtitle,
      this.techDetails,
      this.onReload,
      this.onClose});

  ErrorMessage copyWith({String? title, String? subtitle}) {
    return ErrorMessage(
        title: title ?? this.title,
        subtitle: subtitle ?? this.subtitle,
        techDetails: techDetails,
        onReload: onReload,
        onClose: onClose);
  }

  factory ErrorMessage.forException(BuildContext context, dynamic exception,
      {StackTrace? stackTrace, VoidCallback? onReload, VoidCallback? onClose}) {
    ErrorMessage result;
    if (exception is io.HttpException) {
      result = ErrorMessage(
          title:
          QuadrateLocalization.of(context)?.serverFailure ?? 'Server failure',
          subtitle: QuadrateLocalization.of(context)?.tryAgain ??
              'Please try again in few moments',
          techDetails: 'Exception: $exception \nStacktrace: $stackTrace',
          onReload: onReload,
          onClose: onClose);
    } else if (exception is io.IOException) {
      result = ErrorMessage(
          title: QuadrateLocalization.of(context)?.connectionFailure ??
              'Connection failure',
          subtitle: QuadrateLocalization.of(context)?.checkNetworkAndRetry ??
              'Check network connection and retry',
          techDetails: 'Exception: $exception \nStacktrace: $stackTrace',
          onReload: onReload,
          onClose: onClose);
    } else if (exception is AppException) {
      result = ErrorMessage(
          title: exception.title?.toString(),
          subtitle: exception.description?.toString(),
          techDetails: 'Cause: ${exception.cause} \nStacktrace: $stackTrace',
          onReload: onReload,
          onClose: onClose);
    } else {
      result = ErrorMessage(
          title: QuadrateLocalization.of(context)?.error ?? 'Error',
          subtitle: QuadrateLocalization.of(context)?.tryAgain ??
              'Please try again in few moments',
          techDetails: 'Exception: $exception \nStacktrace: $stackTrace',
          onReload: onReload,
          onClose: onClose);
    }
    return result;
  }

  bool get hasAction {
    return onReload != null || onClose != null;
  }
}
