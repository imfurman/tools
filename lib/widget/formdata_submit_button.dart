import 'package:flutter/material.dart';
import 'package:quadrate_tools/service/form_data.dart';
import 'package:rxdart/rxdart.dart';

class FormDataSubmitButton extends StatelessWidget {
  final FormData formData;
  final VoidCallback? onPressed;
  final String? _caption;
  final Widget? _icon;
  final bool dataShouldChange;
  final bool isTextButton;
  final ButtonStyle? style;

  const FormDataSubmitButton(
      {Key? key,
      required this.formData,
      this.onPressed,
      required String caption,
      this.dataShouldChange = false,
      this.isTextButton = false,
      this.style})
      : _caption = caption,
        _icon = null,
        super(key: key);

  const FormDataSubmitButton.icon(
      {Key? key,
      required this.formData,
      this.onPressed,
      required Widget icon,
      this.dataShouldChange = false,
      this.isTextButton = false,
      this.style})
      : _icon = icon,
        _caption = null,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final captionLocal = _caption;
    final iconLocal = _icon;

    return StreamBuilder<bool>(
        stream: CombineLatestStream.combine3<bool, bool, bool, bool>(
            formData.showValidationMsg.distinct(),
            formData.isValid.distinct(),
            dataShouldChange ? formData.isDataChanged.distinct() : Stream.value(true),
            (showValidationMsgSnapshot, isFormValidSnapshot, isDataChangedSnapshot) {
          return isDataChangedSnapshot &&
              ((showValidationMsgSnapshot != true) || (isFormValidSnapshot == true));
        }),
        builder: (context, btnEnabledSnapshot) {
          final _onPressed = (btnEnabledSnapshot.data == true && onPressed != null)
              ? () {
                  formData.showValidationMsgValue = true;
                  if ((formData.isValid.value == true) && (onPressed != null)) {
                    onPressed!();
                  }
                }
              : null;
          Widget result;

          if (captionLocal != null) {
            result = isTextButton
                ? TextButton(style: style, onPressed: _onPressed, child: Text(captionLocal))
                : ElevatedButton(style: style, onPressed: _onPressed, child: Text(captionLocal));
          } else if (iconLocal != null) {
            result = IconButton.filled(style: style, onPressed: _onPressed, icon: iconLocal);
          } else {
            // should never fall here
            throw Exception('Both caption and icon are null');
          }
          return result;
        });
  }
}

class AlwaysEnabledFormDataSubmitButton extends StatefulWidget {
  final FormData formData;
  final VoidCallback? onPressed;
  final void Function(Object errorMessage)? showErrorMessageCallback;
  final String caption;
  final bool isTextButton;
  final Object combinedErrorMessage;

  const AlwaysEnabledFormDataSubmitButton(
      {Key? key,
      required this.formData,
      this.onPressed,
      required this.caption,
      this.isTextButton = false,
      this.showErrorMessageCallback,
      this.combinedErrorMessage = 'Please fill in all mandatory fields'})
      : super(key: key);

  @override
  State<AlwaysEnabledFormDataSubmitButton> createState() => _AlwaysEnabledFormDataSubmitButtonState();
}

class _AlwaysEnabledFormDataSubmitButtonState extends State<AlwaysEnabledFormDataSubmitButton> {
  late final Stream<Object?> combinedValidationMessage;
  late final VoidCallback _onPressed;
  bool inProgress = false;

  @override
  void initState() {
    super.initState();
    combinedValidationMessage = CombineLatestStream.combine2<bool, Iterable<Object>, Object?>(
        widget.formData.showValidationMsg,
        CombineLatestStream.list<Object?>(widget.formData.controllers
                .map<Stream<Object?>>((controller) => controller.validationMsg))
            .map<Iterable<Object>>(
                (allMessages) => allMessages.where((msg) => msg != null).cast<Object>()),
        (showValidationMsg, nonNullValidationMessages) {
      Object? result;
      if (showValidationMsg && nonNullValidationMessages.isNotEmpty) {
        if (nonNullValidationMessages.length == 1) {
          result = nonNullValidationMessages.first;
        } else {
          result = widget.combinedErrorMessage;
        }
      }
      return result;
    }).asBroadcastStream();

    _onPressed = () async {
      if (!inProgress) {
        inProgress = true;
        try {
          if (widget.formData.isValid.valueOrNull == true) {
            if (widget.onPressed != null) {
              widget.onPressed!();
            }
          } else {
            widget.formData.showValidationMsgValue = true;
            final localShowErrorMessageCallback = widget.showErrorMessageCallback;
            if (localShowErrorMessageCallback != null) {
              // delay is needed for all controllers to process showValidationMsgValue = true
              // there must be better way of doing this
              final validationMsg =
                  await combinedValidationMessage.debounceTime(Duration(milliseconds: 300)).first;
              if (validationMsg != null) {
                localShowErrorMessageCallback(validationMsg);
              }
            }
          }
        } finally {
          inProgress = false;
        }
      }
    };
  }

  @override
  Widget build(BuildContext context) {
    return widget.isTextButton
        ? TextButton(onPressed: _onPressed, child: Text(widget.caption))
        : ElevatedButton(onPressed: _onPressed, child: Text(widget.caption));
  }
}
