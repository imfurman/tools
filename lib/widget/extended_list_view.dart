import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart' as cupertino;
import 'package:flutter/material.dart';
import 'package:quadrate_tools/service/dispose_helper.dart';
import 'package:quadrate_tools/service/helpers.dart';
import 'package:quadrate_tools/widget/error_message_widget.dart';
import 'package:quadrate_tools/widget/extended_state.dart';
import 'package:quiver/collection.dart';
import 'package:rxdart/rxdart.dart';

class ExtendedListView extends StatefulWidget {
  final ExtendedListViewDelegate extendedListViewDelegate;
  final EdgeInsets padding;
  final bool isNested;
  final Widget emptyListMessage;
  final ScrollController? controller;
  final Widget topListItem;
  final RefreshCallback? onRefresh;

  const ExtendedListView(
      {Key? key,
      required this.extendedListViewDelegate,
      this.padding = EdgeInsets.zero,
      this.isNested = false,
      required this.emptyListMessage,
      this.controller,
      this.topListItem = const SizedBox.shrink(),
      this.onRefresh})
      : super(key: key);

  @override
  _ExtendedListViewState createState() => _ExtendedListViewState();
}

class _ExtendedListViewState extends ExtendedState<ExtendedListView> {
  @override
  void initState() {
    super.initState();
    addSubscription(widget.extendedListViewDelegate.errorSubject.listen((errorObj) {
      if (mounted) {
        errorMessage = errorObj == null
            ? null
            : ErrorMessage.forException(context, errorObj, onClose: () {
                errorMessage = null;
                widget.extendedListViewDelegate.error = null;
              });
      }
    }));
  }

  @override
  Widget buildContent(BuildContext context) {
    return StreamBuilder<int?>(
        stream: widget.extendedListViewDelegate.countSubject,
        builder: (context, childCountSnapshot) {
          final childCountSnapshotData = childCountSnapshot.data;
          return () {
            final List<Widget> listContent = childCountSnapshotData != null &&
                    childCountSnapshotData == 0
                ? [
                    SliverToBoxAdapter(
                        child: Padding(
                            padding: EdgeInsets.only(
                                top: widget.padding.top,
                                left: widget.padding.left,
                                right: widget.padding.right),
                            child: widget.topListItem)),
                    SliverFillRemaining(
                        child: Padding(
                            padding: EdgeInsets.only(
                                bottom: MediaQuery.of(context).padding.bottom + widget.padding.bottom,
                                left: widget.padding.left,
                                right: widget.padding.right),
                            child: widget.emptyListMessage))
                  ]
                : [
                    () {
                      final sliverList = SliverList(
                          delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                        return index == 0
                            ? widget.topListItem
                            : widget.extendedListViewDelegate.buildIndexTile(context, index - 1);
                      },
                              childCount:
                                  childCountSnapshotData == null ? null : childCountSnapshotData + 1));

                      return SliverPadding(padding: widget.padding, sliver: sliverList);
                    }()
                  ];
            final customScrollView = CustomScrollView(
                physics: widget.onRefresh != null && Platform.isAndroid
                    ? AlwaysScrollableScrollPhysics()
                    : null,
                controller: widget.controller,
                key: PageStorageKey<String>('extended_list_scroll'),
                slivers: <Widget>[
                  if (widget.onRefresh != null && Platform.isIOS)
                    cupertino.CupertinoSliverRefreshControl(
                      onRefresh: widget.onRefresh,
                    ),
                  if (widget.isNested)
                    SliverOverlapInjector(
                      handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                    ),
                  ...listContent
                ]);
            return widget.onRefresh != null && !Platform.isIOS
                ? RefreshIndicator(child: customScrollView, onRefresh: widget.onRefresh!)
                : customScrollView;
          }();
        });
  }
}

abstract class ExtendedListViewDelegate with DisposeHelper {
  late final BehaviorSubject<int?> _countSubject;

  late final BehaviorSubject<Object?> _errorSubject;

  ExtendedListViewDelegate() {
    addSubject(_countSubject = BehaviorSubject<int?>.seeded(null));
    addSubject(_errorSubject = BehaviorSubject<Object?>.seeded(null));
    addSubscription(_errorSubject.listen((errorValue) {
      if (errorValue == null) {
        refresh();
      }
    }));
  }

  BehaviorSubject<int?> get countSubject => _countSubject;

  BehaviorSubject<Object?> get errorSubject => _errorSubject;

  int? get count => _countSubject.value;

  set count(int? newValue) {
    _countSubject.add(newValue);
  }

  void refresh() {
    _countSubject.add(null);
  }

  set error(Object? error) {
    _errorSubject.add(error);
  }

  Widget buildIndexTile(BuildContext context, int index);
}

abstract class BatchedListViewDelegate<T> extends ExtendedListViewDelegate {
  final LruMap<int, T> cache;

  final DataLoader<T> dataLoader;
  var _loading = false;
  final int batchSize;

  BatchedListViewDelegate(this.dataLoader, {int? cacheSize, required this.batchSize})
      : cache = LruMap<int, T>(maximumSize: cacheSize ?? 100);

  @override
  Widget buildIndexTile(BuildContext context, int index) {
    final data = cache[index];
    if (data == null) {
      _loadData(index);
    }
    return data == null
        ? buildLoadingTile(context)
        : buildDataTile(context, data, (value) {
            cache[index] = value;
            refresh();
          }, prevData: index == 0 ? null : cache[index - 1]);
  }

  Widget buildLoadingTile(BuildContext context);

  Future<void> _loadData(int index) async {
    final offset = (index ~/ batchSize) * batchSize;
    if (!_loading && errorSubject.value == null) {
      _loading = true;
      try {
        final dataLoadResult = await dataLoader(offset, batchSize);
        var index = offset;
        dataLoadResult.items.forEach((element) {
          cache[index] = element;
          index++;
        });
        countSubject.add(dataLoadResult.total);
      } catch (e) {
        error = e;
        rethrow;
      } finally {
        _loading = false;
      }
    }
  }

  Widget buildDataTile(BuildContext context, T data, ValueChanged<T> valueChangedCallback,
      {T? prevData});

  @mustCallSuper
  void clearCache() {
    cache.clear();
    refresh();
  }
}

class TwoSourceListViewDelegate extends ExtendedListViewDelegate {
  ExtendedListViewDelegate firstDelegate;
  ExtendedListViewDelegate secondDelegate;

  TwoSourceListViewDelegate(this.firstDelegate, this.secondDelegate) {
    addSubscription(CombineLatestStream.combine2<int?, int?, int>(
      firstDelegate.countSubject,
      secondDelegate.countSubject,
      (firstCount, secondCount) {
        return (firstCount ?? 1) + (secondCount ?? 1);
      },
    ).listen((combinedCount) {
      countSubject.add(combinedCount);
    }));
    addSubscription(CombineLatestStream.combine2<Object?, Object?, Object?>(
        firstDelegate.errorSubject, secondDelegate.errorSubject, (firstError, secondError) {
      return firstError ?? secondError;
    }).listen((value) {
      errorSubject.add(value);
    }));
  }

  @override
  set error(Object? error) {
    if (error == null) {
      firstDelegate.error = error;
      secondDelegate.error = error;
    } else {
      super.error = error;
    }
  }

  @override
  Widget buildIndexTile(BuildContext context, int index) {
    Widget result;
    final firstCount = firstDelegate.count ?? 1;
    if (index < firstCount) {
      result = firstDelegate.buildIndexTile(context, index);
    } else {
      result = secondDelegate.buildIndexTile(context, index - firstCount);
    }
    return result;
  }
}

typedef DataLoader<D> = Future<EntityListResult<D>> Function(int offset, int batchSize);
