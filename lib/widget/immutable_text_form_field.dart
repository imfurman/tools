import 'package:flutter/material.dart';
import 'package:quadrate_tools/service/helpers.dart';
import 'package:quadrate_tools/service/property_controller.dart';
import 'package:quiver/strings.dart';

typedef ErrorMessageMapper<T> = String? Function(BuildContext context, T? errorMsg);

String? defaultErrorMessageMapper<S>(BuildContext context, S? value) {
  return value?.toString();
}

class ImmutableTextPropertyFormField<V> extends StatelessWidget {
  final PropertyController<dynamic, V> propertyController;
  final InputDecoration _decoration;
  final VoidCallback? onTap;
  final ErrorMessageMapper<V> _errorMessageMapper;
  final TextAlign textAlign;

  ImmutableTextPropertyFormField({
    required this.propertyController,
    InputDecoration? decoration,
    ErrorMessageMapper<V>? errorMessageMapper,
    this.onTap,
    this.textAlign = TextAlign.start,
  })  : _decoration = decoration ?? const InputDecoration(),
        _errorMessageMapper = errorMessageMapper ?? defaultErrorMessageMapper<V>;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String?>(
        stream: propertyController.validationMsg
            .distinct()
            .map((errorMsg) => _errorMessageMapper(context, errorMsg)),
        builder: (context, errorMsgSnapshot) {
          return StreamBuilderHelper<TextSpan>(
              whenEmpty: SizedBox.shrink(),
              stream: propertyController.formatted,
              builder: (context, formattedValue) {
                return ImmutableTextFormField(
                    textAlign: textAlign,
                    value: formattedValue,
                    decoration: _decoration.copyWith(errorText: errorMsgSnapshot.data),
                    onTap: onTap);
              });
        });
  }
}

/// Виджет для отображения нередактируемого текстового поля в форме
class ImmutableTextFormField extends StatelessWidget {
  final TextSpan? value;
  final InputDecoration decoration;
  final VoidCallback? onTap;
  final TextAlign textAlign;

  const ImmutableTextFormField(
      {Key? key,
      this.value,
      this.onTap,
      this.decoration = const InputDecoration(),
      this.textAlign = TextAlign.start})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onTap,
        child: InputDecorator(
            textAlign: textAlign,
            decoration: decoration,
            isEmpty:
                isEmpty(value?.toPlainText(includePlaceholders: false, includeSemanticsLabels: false)),
            child: RichText(text: value ?? TextSpan(text: ''), maxLines: 2, textAlign: textAlign)));
  }
}
