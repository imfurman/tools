import 'dart:async';

import 'package:flutter_test/flutter_test.dart';
import 'package:quadrate_tools/service/form_data.dart';
import 'package:quadrate_tools/service/property_controller.dart';

void main() {
  test('Data changed stream', () async {
    final intPropertyController = PropertyController<int, String>(0);
    final stringPropertyController = PropertyController<String, String>('some string');
    final formData = FormData(
        initControllers: [intPropertyController, stringPropertyController]);
    scheduleMicrotask(() {
      intPropertyController.value = 1;
      stringPropertyController.value = 'some other string';
      intPropertyController.value = 2;
    });
    await expectLater(
        formData.isDataChanged, emitsInOrder(<bool>[false, false, true, true, true]));
  });

  test('Is valid stream', () async {
    final intPropertyController = PropertyController<int, String>(0,
        validator: (value) => value == 1 ? 'error' : null);
    final stringPropertyController = PropertyController<String, String>('zero',
        validator: (value) => value == 'two' ? 'error' : null);
    final formData = FormData(
        initControllers: [intPropertyController, stringPropertyController]);
    scheduleMicrotask(() {
      intPropertyController.value = 1;  // false
      stringPropertyController.value = 'one';  // true
      intPropertyController.value = 2; // true
      stringPropertyController.value = 'two';
      stringPropertyController.value = 'three';
    });
    await expectLater(
        formData.isValid, emitsInOrder(<bool>[true, true, false, false, true, false, true]));
  });
}
