import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:quadrate_tools/widget/currency_form_field.dart';

void main() {
  TextEditingValueWrapper createWrapper(String value) =>
      TextEditingValueWrapper(oldValue: TextEditingValue.empty, newValue: TextEditingValue(text: value));

  test('Find Decimal Digit Position', () {
    expect(CurrencyTextInputFormatter.findDecimalPointPosition(''), -1);
    expect(CurrencyTextInputFormatter.findDecimalPointPosition('10'), -1);
    expect(CurrencyTextInputFormatter.findDecimalPointPosition('0.1'), 1);
    expect(CurrencyTextInputFormatter.findDecimalPointPosition('12,1'), 2);
    expect(CurrencyTextInputFormatter.findDecimalPointPosition('12,1.5'), 2);
    expect(CurrencyTextInputFormatter.findDecimalPointPosition('.'), 0);
  });

  test('Remove Second Decimal Point Rule', () {
    final rule = RemoveSecondDecimalPointRule(null);

    var textEditingValueWrapper = createWrapper('');
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '');

    rule.applyRule(textEditingValueWrapper = createWrapper('0'));
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '0');

    rule.applyRule(textEditingValueWrapper = createWrapper('0.'));
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    expect(textEditingValueWrapper.newValue.text, '0.');

    rule.applyRule(textEditingValueWrapper = createWrapper('0.,'));
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    expect(textEditingValueWrapper.newValue.text, '0.');

    rule.applyRule(textEditingValueWrapper = createWrapper(',.'));
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 0);
    expect(textEditingValueWrapper.newValue.text, ',');

    rule.applyRule(textEditingValueWrapper = createWrapper(',.123'));
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 0);
    expect(textEditingValueWrapper.newValue.text, ',');
  });

  test('Remove Decimal Part Rule', () {
    final rule = RemoveDecimalPartRule(null);

    var textEditingValueWrapper = createWrapper('');
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '');

    textEditingValueWrapper = TextEditingValueWrapper(
        oldValue: TextEditingValue(text: '0.25', selection: TextSelection.collapsed(offset: 1)),
        newValue: TextEditingValue(text: '025', selection: TextSelection.collapsed(offset: 1)));
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '0');

    textEditingValueWrapper = TextEditingValueWrapper(
        oldValue: TextEditingValue(text: '0.25'),
        newValue: TextEditingValue(text: '025', selection: TextSelection.collapsed(offset: 1)));
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '0');

    textEditingValueWrapper = TextEditingValueWrapper(
        oldValue: TextEditingValue(text: '0.25'),
        newValue: TextEditingValue(text: '025', selection: TextSelection.collapsed(offset: 2)));
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '025');

    textEditingValueWrapper = TextEditingValueWrapper(
        oldValue: TextEditingValue(text: '025', selection: TextSelection.collapsed(offset: 1)),
        newValue: TextEditingValue(text: '025', selection: TextSelection.collapsed(offset: 1)));
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '025');

  });

  test('Leading Zeros Rule', () {
    final rule = LeadingZerosRule(null);

    var textEditingValueWrapper = createWrapper('');
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '');

    rule.applyRule(textEditingValueWrapper = createWrapper('0'));
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '0');

    rule.applyRule(textEditingValueWrapper = createWrapper('00'));
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '0');

    rule.applyRule(textEditingValueWrapper = createWrapper('000'));
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '0');

    rule.applyRule(textEditingValueWrapper = createWrapper('1'));
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '1');

    rule.applyRule(textEditingValueWrapper = createWrapper('01'));
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '1');

    rule.applyRule(textEditingValueWrapper = createWrapper('001'));
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '1');

    rule.applyRule(textEditingValueWrapper = createWrapper('0001'));
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '1');

    rule.applyRule(textEditingValueWrapper = createWrapper('0001000'));
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '1000');

    textEditingValueWrapper = createWrapper('.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 0);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    expect(textEditingValueWrapper.newValue.text, '0.');

    textEditingValueWrapper = createWrapper('0.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    expect(textEditingValueWrapper.newValue.text, '0.');

    textEditingValueWrapper = createWrapper('00.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 2);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    expect(textEditingValueWrapper.newValue.text, '0.');

    textEditingValueWrapper = createWrapper('000.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 3);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    expect(textEditingValueWrapper.newValue.text, '0.');

    textEditingValueWrapper = createWrapper('1.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    expect(textEditingValueWrapper.newValue.text, '1.');

    textEditingValueWrapper = createWrapper('100.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 3);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 3);
    expect(textEditingValueWrapper.newValue.text, '100.');

    textEditingValueWrapper = createWrapper('0100.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 4);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 3);
    expect(textEditingValueWrapper.newValue.text, '100.');

    textEditingValueWrapper = createWrapper('00100.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 5);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 3);
    expect(textEditingValueWrapper.newValue.text, '100.');

    textEditingValueWrapper = createWrapper('000100.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 6);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 3);
    expect(textEditingValueWrapper.newValue.text, '100.');
  });

  test('Decimal Part Rule', () {
    final rule = DecimalPartRule(null);

    var textEditingValueWrapper = createWrapper('');
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '');

    rule.applyRule(textEditingValueWrapper = createWrapper('0'));
    expect(textEditingValueWrapper.newValueDecimalPointPosition, -1);
    expect(textEditingValueWrapper.newValue.text, '0');

    textEditingValueWrapper = createWrapper('.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 0);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 0);
    expect(textEditingValueWrapper.newValue.text, '.00');

    textEditingValueWrapper = createWrapper('0.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    expect(textEditingValueWrapper.newValue.text, '0.00');

    textEditingValueWrapper = createWrapper('00.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 2);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 2);
    expect(textEditingValueWrapper.newValue.text, '00.00');

    textEditingValueWrapper = createWrapper('000.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 3);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 3);
    expect(textEditingValueWrapper.newValue.text, '000.00');

    textEditingValueWrapper = createWrapper('1.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    expect(textEditingValueWrapper.newValue.text, '1.00');

    textEditingValueWrapper = createWrapper('1.1');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    expect(textEditingValueWrapper.newValue.text, '1.10');

    textEditingValueWrapper = createWrapper('1.11');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    expect(textEditingValueWrapper.newValue.text, '1.11');

    textEditingValueWrapper = createWrapper('100.');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 3);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 3);
    expect(textEditingValueWrapper.newValue.text, '100.00');

    textEditingValueWrapper = createWrapper('1.123');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    expect(textEditingValueWrapper.newValue.text, '1.12');

    textEditingValueWrapper = createWrapper('1.12345');
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    rule.applyRule(textEditingValueWrapper);
    expect(textEditingValueWrapper.newValueDecimalPointPosition, 1);
    expect(textEditingValueWrapper.newValue.text, '1.12');
  });

  test('String To Int Transformer', () {
    expect(CurrencyFormField.stringToIntTransformer(''), null);
    expect(CurrencyFormField.stringToIntTransformer('a'), null);
    expect(CurrencyFormField.stringToIntTransformer('a.b'), null);
    expect(CurrencyFormField.stringToIntTransformer('a,b'), null);
    expect(CurrencyFormField.stringToIntTransformer('0a,b'), null);
    expect(CurrencyFormField.stringToIntTransformer('0a,1b'), null);
    expect(CurrencyFormField.stringToIntTransformer('0a,1'), null);
    expect(CurrencyFormField.stringToIntTransformer('0,1b'), null);
    expect(CurrencyFormField.stringToIntTransformer('0,1'), 10);
    expect(CurrencyFormField.stringToIntTransformer('0,10'), 10);
    expect(CurrencyFormField.stringToIntTransformer('0,12'), 12);
    expect(CurrencyFormField.stringToIntTransformer('0,123'), 12);
    expect(CurrencyFormField.stringToIntTransformer('1,234'), 123);
    expect(CurrencyFormField.stringToIntTransformer('1,2.3'), null);
    expect(CurrencyFormField.stringToIntTransformer('1,.3'), null);
  });

  test('Int To String Transformer', () {
    expect(CurrencyFormField.intToStringTransformer(null), null);
    expect(CurrencyFormField.intToStringTransformer(0), '0');
    expect(CurrencyFormField.intToStringTransformer(1), '0.01');
    expect(CurrencyFormField.intToStringTransformer(01), '0.01');
    expect(CurrencyFormField.intToStringTransformer(10), '0.10');
    expect(CurrencyFormField.intToStringTransformer(12), '0.12');
    expect(CurrencyFormField.intToStringTransformer(100), '1');
    expect(CurrencyFormField.intToStringTransformer(120), '1.20');
    expect(CurrencyFormField.intToStringTransformer(123), '1.23');
  });
}
